The application Bio7 is an integrated development environment for ecological modeling, statistical and scientific image analysis. The application itself is based on an RCP-Eclipse-Environment (Rich-Client-Platform) which offers a huge flexibility in configuration and extensibility because of its plug-in structure and the possibility of customization.

Features:

*     Creation and analysis of simulation models.
*     Statistical analysis.
*     Advanced R Graphical User Interface with editor, spreadsheet, ImageJ plot device and debugging interface.
*     Spatial statistics (possibility to send values from a specialized panel to R).
*     Image Analysis (embedded ImageJ).
*     Fast transfer of image data from ImageJ to R and vice versa.
*     Fast communication between R and Java (with RServe) and the possibilty to use R methods inside Java.
*     Interpretation of Java and script creation (BeanShell, Groovy, Jython).
*     Dynamic compilation of Java.
*     Creation of methods for Java, BeanShell, Groovy, Jython and R (integrated editors for Java, R, BeanShell, Groovy, Jython).
*     Sensitivity analysis with an embedded flowchart editor in which scripts, macros and compiled code can be dragged and executed.
*     Creation of 3d OpenGL (Jogl) models.
*     Visualizations and simulations on an embedded 3d globe (World Wind Java SDK).
*     Creation of Graphical User Interfaces with the embedded JavaFX SceneBuilder.